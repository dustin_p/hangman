public class Hangman {
    
    public static void main(String[] args) {
        Game game = new Game("treehouse"); /* Created a new game with the word Treehouse */
        Prompter prompter = new Prompter(game); /* Passed in the game to the prompter */
        boolean isHit = prompter.promptForGuess(); /* prompted for gues */
        if (isHit) {                                 
            System.out.println("We got a hit!");    /* Pulled info wether it was hit or miss and printed out info */
        } else {
            System.out.println("That was a miss!");
        }
        
    }     
}