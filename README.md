# README #

## What this is ##

This is a basic java application I'm using to learn how to program Java using Treehouse. Hopefully by the end of the creation of this simple hangman style game, I'll have a better understanding of Java programming.

## Who I am ##

I'm a beer geek that has gotten into programming a bit later in life than I should have. I'm hoping to better myself by learning Java and eventually Android programming.  You'll find a lot of comments within this program that are meant to be reminders to myself, as to what each portion is doing. 