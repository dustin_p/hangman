import java.io.Console;

public class Prompter {
	private Game mGame; 	/* gives a place to store Game named mGame */
	
	public Prompter(Game game) {  
		 mGame = game;		/* if you create a prompter, you will give it a game object which will be stored in mGame */
	}
	
	public boolean promptForGuess() { 
		Console console = System.console();	/* this will prompt for a guess using true/false (boolean) */
		String guessAsString = console.readLine("Enter a letter:   ");
		char guess = guessAsString.charAt(0); /* takes the first guess entered */
		return mGame.applyGuess(guess); /* calls the applyGuess method and passes in the guess we got in line 13 */
	}
}